import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { StudentComponent } from './components/student/student.component';
import { SubjectComponent } from './components/subject/subject.component';
import { TeacherComponent } from './components/teacher/teacher.component';


const routes: Routes = [
  {path:'Home',component:HomeComponent},
  {path:'',component:LoginComponent},
  {path:'Teacher',component:TeacherComponent},
  {path:'Student',component:StudentComponent},
  {path:'Subject',component:SubjectComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
