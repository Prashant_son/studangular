import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private http:HttpClient) { }
  
  getRequest(){
  //   let headers = new HttpHeaders();
  //   headers.set('Access-Control-Allow-Origin','*');
  //   this.http.get('http://localhost:8080/',{headers:headers}).subscribe((res)=>{
  //     console.log(res);
  //     return res;
  // });
  return new Promise((resolve,reject) => {
      this.http.get('http://localhost:8080/').subscribe(res=>{
        resolve(res)
      },(err) => {
        reject(err)
      }); 
    });
  };

}
