import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  Ngform:any = {};
  error:any = {};
  user : any = {username:"XYZ"};
  constructor(public router:Router) { }

  ngOnInit(): void {
  }

  login(){
    if(this.user.username == "abc" && this.user.password == "123"){
        alert("Login Successfull")
        this.router.navigate(['Home']);
    }else {
      alert("Login Failed")
    }
  }
}
