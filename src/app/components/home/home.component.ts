import { Component, OnInit } from '@angular/core';
import { UserServiceService } from 'src/app/services/user-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  userList:any = [];
  userObjectList:any = {};
  constructor(private userService:UserServiceService) {
     this.getUser();
    //this.uList = this.userList.object;
     //alert(this.userList);
   }

  ngOnInit(): void {
  }
  
  getUser(){re
    this.userService.getRequest().then((result)=> {
      this.userList = result;
      this.userObjectList = this.userList.object;
      alert(JSON.stringify(result));
      //alert(JSON.stringify(this.userObjectList));
    });
  }

}
